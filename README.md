# latex-optimizer

Implementation from a blog post [LaTeX writing as a constrained non-convex optimization problem](https://blog.martisak.se/2020/06/06/latex-optimizer/).

To run:

```
pip install -r requirements.txt
python optimizer.py
```
